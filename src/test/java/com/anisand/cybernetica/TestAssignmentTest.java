package com.anisand.cybernetica;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static com.anisand.cybernetica.TestAssignment.convert;
import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
public class TestAssignmentTest {
    @Test
    public void test1() {
        assertThat(convert("(A,B) (B,C) (B,D) (D,E)")).isEqualTo("(A(B(C)(D(E))))");
    }

    @Test
    public void test2() {
        assertThat(convert("(D,E) (B,D) (A,B) (B,C)")).isEqualTo("(A(B(C)(D(E))))");
    }

    @Test
    public void test3() {
        assertThat(convert("(A,B)")).isEqualTo("(A(B))");
    }

    @Test
    public void test_empty_input() {
        assertThat(convert("")).isEqualTo("()");
    }

    @Test
    public void test_E1_multiple_roots() {
        assertThat(convert("(A,B) (C,D)")).isEqualTo("E1");
    }

    @Test
    public void test_E2_cycle_present() {
        assertThat(convert("(A,B) (B,A)")).isEqualTo("E2");
    }

    @Test
    public void test_E2_cycle_present_2() {
        assertThat(convert("(A,B) (B,C) (C,B)")).isEqualTo("E2");
    }

    @Test
    public void test_E2_cycle_present_3() {
        assertThat(convert("(A,B) (B,C) (B,D) (C,D)")).isEqualTo("E2");
    }

    @Test
    public void test_E2_cycle_present_4() {
        assertThat(convert("(A,B) (B,C) (A,D) (D,E) (B,E)")).isEqualTo("E2");
    }

    @Test
    public void test_E2_cycle_present_5() {
        assertThat(convert("(A,A)")).isEqualTo("E2");
    }

    @Test
    public void test_E3_duplicate_node() {
        assertThat(convert("(A,B) (A,B)")).isEqualTo("E3");
        assertThat(convert("(A,B) (B,C) (B,C)")).isEqualTo("E3");
    }

    @Test
    public void test_E4_too_many_children() {
        assertThat(convert("(A,B) (A,C) (A,D)")).isEqualTo("E4");
    }

    @Test
    public void test_E5_general_error_1() {
        assertThat(convert("() ()")).isEqualTo("E5");
    }

    @Test
    public void test_E5_general_error_2() {
        assertThat(convert("(A) (A)")).isEqualTo("E5");
    }

    @Test
    public void test_E5_general_error_3() {
        assertThat(convert("(A) (B)")).isEqualTo("E5");
    }
}
